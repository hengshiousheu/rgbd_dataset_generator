# RGB-T 資料集產生器
## 項目說明
本專案旨在建立具有 RGB-D 資料集，提供給 RGB-D 演算法來進行訓練，目標是產出同一個時間、角度下的 RGB 與 Thermal 影像。

## 硬體要求
本次示範使用以下清單
- Pi4
- Pi Camera
- FLIR Lepton 2.5(80x60)

## 檔案結構說明
 - img_generator : 同時產出 RGB 影像與 Thermal 影像檔案
 - utils : 提供 helper function 給主程式使用
## 使用說明
1. 下載專案
2. 打開 CML
3. cd 至專案位置
4. 執行指令 `$ python2 img_generator.py`
5. 得到名為 "generated_images" 資料夾，裡頭存有兩張圖片，檔案名稱分別為<拍攝時間>-lepton_img.jpg 與<拍攝時間>-pi_img.jpg

## 效果呈現
![Lepton_image](demo/2020_10_06-09_49_56_PM-lepton_img.jpg)

![RGB_image](demo/2020_10_06-09_49_56_PM-pi_img.jpg)
## 使用函式庫
- [picamera](https://picamera.readthedocs.io/en/release-1.13/install.html)
- [pylepton](https://github.com/groupgets/pylepton)
- [cv2]()
- [numpy]()

## 等待增加功能
- []也許有
- []也許沒有
- []隨緣功能
