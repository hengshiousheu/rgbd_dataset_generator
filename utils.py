import numpy as np
import cv2
import os

# import the necessary packages
from pylepton import Lepton
from picamera.array import PiRGBArray
from picamera import PiCamera

def make_img_folder(desired_path = "generated_images"):
    dest_path = os.path.join(desired_path)
    if not os.path.isdir(dest_path):
        print("Generate folder: ", dest_path)
        os.mkdir(dest_path)

def capture_Picamera():
    """
    :Desc: Capture the image from picamera
    :return: BGR image array
    """
    camera = PiCamera()
    rawCapture = PiRGBArray(camera)
    camera.capture(rawCapture, format="bgr")
    return rawCapture.array

def write_image(image_src = None, dest = './', file_name = 'right.jpg'):
    """
    :Desc: Capture the image from picamera
    :param image_src: 
    :param dest:
    :param file_name:
    :return: BGR image array
    """
    path = os.path.join(dest, file_name)
    cv2.imwrite(path, image_src)
    print("write Name: ", file_name, " into ", path)

def read_image(imFilename = None, imread = cv2.IMREAD_COLOR):
    """
    :Desc: Read the image from @imFilename and savein var
    :param imFilename: image location and name(ex: current/path/sayMyName.jpg) 
    :param imread: Parameterss for CV2
    :return: image array
    """
    read_img = cv2.imread(imFilename, imread)
    print("Reading image : ", imFilename);  
    return read_img

def raw_to_8bit(data):
    """
    :Desc: # normalize to 0-255 # convert to uint8
    :param data: 
    :return:
    """
    cv2.normalize(data, data, 0, 255, cv2.NORM_MINMAX)
    return np.uint8(data)

def ktoc(val):
    # 16-bit Kelvin to deg Celsius
    return (val - 27315) / 100.0

def capture_lepton(flip_v = False, device = "/dev/spidev0.0"):
    """
    :Desc: Capture the image from Lepton module
    :param flip_v: flip the output image vertically, True or False
    :param device: specify the spi device node (might be /dev/spidev0.1 on a newer device)
    :return: gray image in Datatype(uint8)
    """
    with Lepton(device) as l:
        data, _ = l.capture()
    if flip_v:
        cv2.flip(data, 0, data)

    minVal, maxVal, _, _ = cv2.minMaxLoc(data)
    resized_data = cv2.resize(ktoc(data[:,:]), (800, 480))
    _, _, minLoc, maxLoc = cv2.minMaxLoc(resized_data)
    print("Lepton: ", ktoc(minVal), ktoc(maxVal), minLoc, maxLoc)
    resized_data = raw_to_8bit(resized_data)
    return (data, resized_data, minVal, maxVal, minLoc, maxLoc)