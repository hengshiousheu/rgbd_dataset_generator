from picamera.array import PiRGBArray
from picamera import PiCamera
from utils import *
from datetime import datetime

if __name__ == "__main__":
    des_path = "./generated_images"
    make_img_folder(des_path)

    current_time = datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")
    
    (data, image, minVal, maxVal, minLoc, maxLoc) = capture_lepton()
    ir_img = image
    pi_img = capture_Picamera()
    
    write_image(image_src = ir_img, file_name = current_time + '-lepton_img.jpg', dest=des_path)
    write_image(image_src = pi_img, file_name = current_time + '-pi_img.jpg',dest=des_path)
    